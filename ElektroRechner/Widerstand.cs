﻿using System;


namespace ElektroRechner
{
    class Widerstand
    {
        private double _i;
        private double _p;
        private double _u;
        public double i
        {
            get { return _i; }
            set
            {
                if (value >= 0) { _i = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }
        public double p
        {
            get { return _p; }
            set
            {
                if (value >= 0) { _p = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }
        public double u
        {
            get { return _u; }
            set
            {
                if (value >= 0) { _u = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }
        public string menu = "Welche Werte sind verfügbar?\n 1 - Stromstärke (I) und Spannung (U)\n 2 - Leistung (P) und Spannung (U)\n 3 - Stromstärke (I) und Leistung (P)";

        public double berechnen()
        {
            double ergebnis = 0;
            if (p > 0 && u > 0)
            {
                ergebnis = (u * u) / p;
            }
            else if (i > 0 && u > 0)
            {
                ergebnis = Convert.ToDouble(u / i);
            }
            else if (p > 0 && i > 0)
            {
                ergebnis = p / (i * i);
            }
            else
            {
                throw new ArgumentNullException("Es fehlen Parameter");
            }
            return ergebnis;
        }

        public void erfassen()
        {
            Console.Clear();
            Console.WriteLine(menu);
            int tmp = Helper.IntegerInput("Auswahl:");
            if (tmp == 1)
            {
                u = Helper.DoubleInput("Spannung:");
                i = Helper.DoubleInput("Stromstärke:");
            }
            else if (tmp == 2)
            {
                p = Helper.DoubleInput("Leistung:");
                u = Helper.DoubleInput("Spannung");
            }
            else if (tmp == 3)
            {
                i = Helper.DoubleInput("Stromstärke:");
                p = Helper.DoubleInput("Leistung:");
            }
            Console.WriteLine("Widerstand: " + berechnen() + " Ohm");
            Program.Menu();
        }

    }
}
