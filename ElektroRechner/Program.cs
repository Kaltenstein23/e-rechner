﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElektroRechner
{
    class Program
    {
        public static void Menu()
        {
            string menu = "Bitte wählen Sie aus:\n1 - Spannung (U) berechnen\n2 - Stromstärke (I) berechnen\n3 - Widerstand (R) berechnen\n4 - Leistung (P) berechnen\n5 - Beenden\n\n";
            Console.Write(menu);
            int tmp = Helper.IntegerInput("Auswahl: ");
            switch (tmp)
            {
                case 1:
                    Spannung U = new Spannung();
                    U.erfassen();                    
                    break;
                case 2:
                    Stromstaerke I = new Stromstaerke();
                    I.erfassen();
                    break;
                case 3:
                    Widerstand R = new Widerstand();
                    R.erfassen();
                    break;
                case 4:
                    Leistung P = new Leistung();
                    P.erfassen();
                    break;
                case 5:
                    Console.ReadKey();
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("dude, veralber mich nicht...");
                    Menu();
                    break;
            }
        }

        static void Main(string[] args)
        {
                Menu();
        }
    }
}
