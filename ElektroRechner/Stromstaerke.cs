﻿using System;


namespace ElektroRechner
{
    class Stromstaerke
    {
        private double _r;
        private double _p;
        private double _u;
        public double r
        {
            get { return _r; }
            set
            {
                if (value >= 0) { _r = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }
        public double p
        {
            get { return _p; }
            set
            {
                if (value >= 0) { _p = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }
        public double u
        {
            get { return _u; }
            set
            {
                if (value >= 0) { _u = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }

        public string menu = "Welche Werte sind bekannt?\n 1 - Widerstand (R) und Leistung (P)\n 2 - Leistung (P) und Spannung (U)\n 3 - Widerstand (R) und Spannung (U)\n\n"; //Die Klasse hat einen String menu mit dem angegebenen Inhalt


        // Die Klasse 
        public double berechnen()
        {
            double ergebnis = 0; // Die Funktion gibt ein Ergebnis aus.
            if (r > 0 && p > 0) // Variable r und p sind nicht leer
            {
                ergebnis = Math.Sqrt(r*p); //Multipliziere die beiden Variablen mit einander, und berechne die Quadratwurzel daraus, weise es der Variablen ergebnis zu
            }
            else if (r > 0 && u > 0)
            {
                ergebnis = u / r;
            }
            else if (p > 0 && u > 0)
            {
                ergebnis = p / u;
            }
            else
            {
                throw new ArgumentNullException("Dir fehlen Parameter."); // Da fehlt doch was
            }
            return ergebnis; // Gib das Ergebnis zurück
        }

        public void erfassen()
        {
            Console.Clear();
            Console.WriteLine(menu);
            int tmp = Helper.IntegerInput("Auswahl:");
            if (tmp == 1)
            {
                r = Helper.DoubleInput("Widerstand:");
                p = Helper.DoubleInput("Leistung:");
            }
            else if (tmp == 2)
            {
                p = Helper.DoubleInput("Leistung:");
                u = Helper.DoubleInput("Spannung:");
            }
            else if (tmp == 3)
            {
                r = Helper.DoubleInput("Widerstand:");
                u = Helper.DoubleInput("Spannung:");
            }
            Console.WriteLine("Stromstärke: " + berechnen() + " Ampere");
            Program.Menu();
        }
    }
}
