﻿using System;


namespace ElektroRechner
{
    class Spannung
    {
        private double _i;
        private double _p;
        private double _r;
        public double i
        {
            get { return _i; }
            set
            {
                if (value >= 0) { _i = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }
        public double p
        {
            get { return _p; }
            set
            {
                if (value >= 0) { _p = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }
        public double r
        {
            get { return _r; }
            set
            {
                if (value >= 0) { _r = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }
        public string menu = "Welche Werte sind bekannt?\n\n1 - Widerstand (R) und Stromstärke (I)\n2 - Leistung (P) und Stromstärke (I)\n3 - Leistung (P) und Widerstand(R)\n\n";
        
        public double berechnen()
        {
            double ergebnis = 0;
            if (r > 0 && i > 0)
            {
                ergebnis = r * i;
            }
            else if (p > 0 && i > 0)
            {
                ergebnis = p / i;
            }
            else if (r > 0 && p > 0)
            {
                ergebnis = Math.Sqrt(p * r);
            }
            else
            {
                throw new ArgumentOutOfRangeException("Methode hat einen ungültigen Wert");
            }
            return ergebnis;
        }

        public void erfassen()
        {
            Console.Clear();
            Console.WriteLine(menu);
            int tmp = Helper.IntegerInput("Auswahl:");
            if (tmp == 1)
            {
                r = Helper.DoubleInput("Widerstand:");
                i = Helper.DoubleInput("Stromstärke");
            }
            else if (tmp == 2)
            {
                p = Helper.DoubleInput("Leistung:");
                i = Helper.DoubleInput("Stromstärke");
            }
            else if (tmp == 3)
            {
                r = Helper.DoubleInput("Widerstand:");
                p = Helper.DoubleInput("Leistung:");
            }
            Console.WriteLine("Spannung: " + berechnen() + " Volt");
            Program.Menu();
        }
    }
}
