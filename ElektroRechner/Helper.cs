﻿using System;


namespace ElektroRechner
{
    class Helper
    {
        public static Int32 IntegerInput(string msg)
        {
            Int32 tmp = 0;
            try
            {
                tmp = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException e)
            {
                Console.WriteLine("Du hast keine Zahl angegeben! Versuch es erneut!");
                IntegerInput(msg);
            }
            catch (OverflowException e)
            {
                Console.WriteLine("Du hast eine zu große Zahl angegeben! Versuch es erneut!");
                IntegerInput(msg);
            }
            catch (Exception e)
            {
                Console.WriteLine("Huch, deinem Schicksal ist ein Fehler unterlaufen. Gib deinem Wahrsager diese Nachricht: " + e);
                IntegerInput(msg);
            }

            return tmp;

        }

        public static double DoubleInput(string msg)
        {
            double tmp = 0;
            try
            {
                Console.WriteLine(msg);
                tmp = Convert.ToDouble(Console.ReadLine());
            }
            catch (FormatException e)
            {
                Console.WriteLine("Du hast keine korrekte Zahl angegeben! Versuch es erneut!");
                DoubleInput(msg);
            }
            catch (OverflowException e)
            {
                Console.WriteLine("Du hast dem Schicksal eine zu groÃe Zahl gegeben! Versuch es erneut!");
                DoubleInput(msg);
            }
            catch (Exception e)
            {
                Console.WriteLine("Huch, deinem Schicksal ist ein Fehler unterlaufen. Gib deinem Wahrsager diese Nachricht: " + e);
                DoubleInput(msg);
            }
            return tmp;
        }

        public static string StringInput(string msg)
        {
            string tmp = "";
            try
            {
                tmp = Convert.ToString(Console.ReadLine());
            }
            catch (FormatException e)
            {
                Console.WriteLine("Du hast keinen korrekten String eingegeben! Versuch es erneut!");
                StringInput(msg);
            }
            catch (OverflowException e)
            {
                Console.WriteLine("Du hast einen zu großen String eingegeben! Versuch es erneut!");
                StringInput(msg);
            }
            catch (Exception e)
            {
                Console.WriteLine("Huch, deinem Schicksal ist ein Fehler unterlaufen. Gib deinem Wahrsager diese Nachricht: " + e);
                StringInput(msg);
            }
            return tmp;
        }
    }
}
