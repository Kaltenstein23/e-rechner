﻿using System;


namespace ElektroRechner
{
    class Leistung
    {
        private double _i;
        private double _r;
        private double _u;
        public double i
        {
            get { return _i; }
            set
            {
                if (value >= 0) { _i = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }
        public double r
        {
            get { return _r; }
            set
            {
                if (value >= 0) { _r = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }
        public double u
        {
            get { return _u; }
            set
            {
                if (value >= 0) { _u = value; }
                else { throw new ArgumentOutOfRangeException("Unzulässiger Wert"); }
            }
        }
        public string menu = "Welche Werte sind verfügbar?\n 1 - Stromstärke (I) und Spannung (U)\n 2 - Widerstand (R) und Spannung (U)\n 3 - Stromstärke (I) und Widerstand (R)";

        public double berechnen()
        {
            double ergebnis = 0;
            if (r > 0 && u > 0)
            {
                ergebnis = (u * u) / r;
            }
            else if (i > 0 && u > 0)
            {
                ergebnis = u * i;
            }
            else if (r > 0 && i > 0)
            {
                ergebnis = r * (i * i);
            }
            else
            {
                throw new ArgumentNullException("Es fehlen Parameter");
            }
            return ergebnis;
        }

        public void erfassen()
        {
            Console.Clear();
            Console.WriteLine(menu);
            int tmp = Helper.IntegerInput("Auswahl:");
            if (tmp == 1)
            {
                u = Helper.DoubleInput("Spannung:");
                i = Helper.DoubleInput("Stromstärke:");
            }
            else if (tmp == 2)
            {
                r = Helper.DoubleInput("Widerstand:");
                u = Helper.DoubleInput("Spannung");
            }
            else if (tmp == 3)
            {
                i = Helper.DoubleInput("Stromstärke:");
                r = Helper.DoubleInput("Widerstand:");
            }
            Console.WriteLine("Leistung: " + berechnen() + " Watt");
            Program.Menu();
        }

    }
}
